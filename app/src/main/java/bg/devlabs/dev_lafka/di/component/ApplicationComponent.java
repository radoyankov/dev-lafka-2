package bg.devlabs.dev_lafka.di.component;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import bg.devlabs.dev_lafka.Devlafka;
import bg.devlabs.dev_lafka.data.DataManager;
import bg.devlabs.dev_lafka.di.ApplicationContext;
import bg.devlabs.dev_lafka.di.module.ApplicationModule;
import bg.devlabs.dev_lafka.service.SyncService;
import bg.devlabs.dev_lafka.ui.bill.BillFragment;
import bg.devlabs.dev_lafka.ui.home.HomeFragment;
import bg.devlabs.dev_lafka.ui.main.MainActivity;
import bg.devlabs.dev_lafka.ui.login.LoginFragment;
import bg.devlabs.dev_lafka.ui.shortcut.ShortcutActivity;
import dagger.Component;

/**
 * Created by Radoslav on 06-Mar-17.
 */

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

	Devlafka inject(Devlafka app);

	SyncService inject(SyncService service);

	LoginFragment inject(LoginFragment fragment);

	HomeFragment inject(HomeFragment fragment);

	MainActivity inject(MainActivity mainActivity);

	ShortcutActivity inject(ShortcutActivity shortcutActivity);

	BillFragment inject(BillFragment billFragment);

	@ApplicationContext
	Context context();

	Application application();

	DataManager getDataManager();

}