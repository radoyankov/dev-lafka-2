package bg.devlabs.dev_lafka.di.component;

import bg.devlabs.dev_lafka.di.PerService;
import bg.devlabs.dev_lafka.di.module.ServiceModule;
import bg.devlabs.dev_lafka.service.SyncService;
import dagger.Component;

/**
 * Created by Radoslav on 06-Mar-17.
 */

@PerService
@Component(dependencies = ApplicationComponent.class, modules = ServiceModule.class)
public interface ServiceComponent {

	void inject(SyncService service);

}
