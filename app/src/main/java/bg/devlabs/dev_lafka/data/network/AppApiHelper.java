package bg.devlabs.dev_lafka.data.network;


import java.util.List;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.data.network.format.ApiResponse;
import bg.devlabs.dev_lafka.data.network.model.LogResponse;
import bg.devlabs.dev_lafka.data.network.model.MakeResponse;
import bg.devlabs.dev_lafka.data.network.model.OrdersResponse;
import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import io.reactivex.Observable;
import retrofit2.Retrofit;


/**
 * Created by Radoslav on 06-Mar-17.
 */

public class AppApiHelper implements ApiHelper {

	TaskService taskService;

	@Inject
	public AppApiHelper(Retrofit retrofit) {
		taskService = retrofit.create(TaskService.class);
	}

	@Override
		public Observable<ApiResponse<LogResponse>> doLoginCall(String email, String password) {
		return taskService.login(email, password);
	}

	@Override
	public Observable<ApiResponse<MakeResponse>> doMakeCall(String session_id, int productId, int quantity) {
		return taskService.makeOrder(session_id, productId, quantity);
	}

	@Override
	public Observable<ApiResponse> doLogoutCall(String session_id) {
		return taskService.logout(session_id);
	}

	@Override
	public Observable<ApiResponse<List<ProductsResponse>>> doProductsCall(String session_id) {
		return taskService.getProducts(session_id);
	}

	@Override
	public Observable<ApiResponse<List<OrdersResponse>>> doOrdersCall(String session_id, String lastUpdated) {
		return taskService.getUsersOrders(session_id, lastUpdated);
	}

	@Override
	public Observable<ApiResponse> doDeleteCall(String session_id, int id) {
		return taskService.deleteOrder(session_id, id);
	}


}
