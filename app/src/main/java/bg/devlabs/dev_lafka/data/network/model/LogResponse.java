package bg.devlabs.dev_lafka.data.network.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Radoslav on 06-Mar-17.
 */

public class LogResponse {

	@SerializedName("id")
	@Expose
	private String id;
	@SerializedName("email")
	@Expose
	private String email;
	@SerializedName("first_name")
	@Expose
	private String firstName;
	@SerializedName("last_name")
	@Expose
	private String lastName;
	@SerializedName("phone")
	@Expose
	private String phone;
	@SerializedName("confirmation_code")
	@Expose
	private String confirmationCode;
	@SerializedName("confirmed")
	@Expose
	private String confirmed;
	@SerializedName("role_id")
	@Expose
	private String roleId;
	@SerializedName("favourite_product_id")
	@Expose
	private Object favouriteProductId;
	@SerializedName("session_id")
	@Expose
	private String sessionId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getConfirmationCode() {
		return confirmationCode;
	}

	public void setConfirmationCode(String confirmationCode) {
		this.confirmationCode = confirmationCode;
	}

	public String getConfirmed() {
		return confirmed;
	}

	public void setConfirmed(String confirmed) {
		this.confirmed = confirmed;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public Object getFavouriteProductId() {
		return favouriteProductId;
	}

	public void setFavouriteProductId(Object favouriteProductId) {
		this.favouriteProductId = favouriteProductId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
}
