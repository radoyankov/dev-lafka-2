package bg.devlabs.dev_lafka.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.di.ApplicationContext;
import bg.devlabs.dev_lafka.di.PreferenceInfo;

/**
 * Created by Radoslav on 06-Mar-17.
 */

public class AppPreferencesHelper implements PreferencesHelper{
	private static final String PREF_KEY_CURRENT_USER_NAME = "PREF_KEY_CURRENT_USER_NAME";
	private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
	private static final String PREF_KEY_LAST_ITEM = "PREF_KEY_LAST_ITEM";

	private final SharedPreferences mPrefs;

	@Inject
	public AppPreferencesHelper(@ApplicationContext Context context,
								@PreferenceInfo String prefFileName) {
		mPrefs = context.getSharedPreferences(prefFileName, Context.MODE_PRIVATE);
	}

	@Override
	public String getCurrentUserName() {
		return mPrefs.getString(PREF_KEY_CURRENT_USER_NAME, "");
	}

	@Override
	public void setCurrentUserName(String name) {
		mPrefs.edit().putString(PREF_KEY_CURRENT_USER_NAME, name).apply();
	}

	@Override
	public String getAccessToken() {
		return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, "");
	}

	@Override
	public void setAccessToken(String accessToken) {
		mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
	}

	@Override
	public String getLastItem() {
		return mPrefs.getString(PREF_KEY_LAST_ITEM, "1");
	}

	@Override
	public void setLastItem(String item) {
		mPrefs.edit().putString(PREF_KEY_LAST_ITEM, item).apply();
	}
}
