package bg.devlabs.dev_lafka.data;

import bg.devlabs.dev_lafka.data.network.ApiHelper;
import bg.devlabs.dev_lafka.data.prefs.PreferencesHelper;

/**
 * Created by Radoslav on 06-Mar-17.
 */

public interface DataManager extends ApiHelper, PreferencesHelper{
	void updateUserInfo (String accessToken, String name);
}
