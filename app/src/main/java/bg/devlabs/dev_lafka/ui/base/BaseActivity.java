package bg.devlabs.dev_lafka.ui.base;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import bg.devlabs.dev_lafka.Devlafka;
import bg.devlabs.dev_lafka.R;
import bg.devlabs.dev_lafka.di.component.ApplicationComponent;
import bg.devlabs.dev_lafka.utils.NetworkUtils;
import butterknife.Unbinder;

/**
 * Created by Radoslav on 08-Mar-17.
 *
 * Base Activity
 */

public abstract class BaseActivity extends AppCompatActivity
		implements LafkaView,  BaseFragment.Callback {



//	private ActivityComponent mActivityComponent;
	private ApplicationComponent mApplicationComponent;
	private Unbinder mUnBinder;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mApplicationComponent = Devlafka.getAppComponent(this);
//		mActivityComponent = DaggerActivityComponent.builder()
//				.activityModule(new ActivityModule(this))
//				.applicationComponent(((Devlafka) getApplication()).getComponent())
//				.build();
	}

//	public ActivityComponent getActivityComponent() {
//		return mActivityComponent;
//	}

	public ApplicationComponent getmApplicationComponent(){
		return mApplicationComponent;
	}


	@TargetApi(Build.VERSION_CODES.M)
	public void requestPermissionsSafely(String[] permissions, int requestCode) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
			requestPermissions(permissions, requestCode);
		}
	}

	@TargetApi(Build.VERSION_CODES.M)
	public boolean hasPermission(String permission) {
		return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
				checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
	}

	@Override
	public void onFragmentAttached() {

	}

	@Override
	public void onFragmentDetached(String tag) {

	}

	@Override
	public void openActivityOnTokenExpire() {

	}

	@Override
	public void onError(@StringRes int resId) {
		onError(getString(resId));

	}

	/**
	 * Displays a snackbar with a message
	 * @param message The message to be displayed
	 */
	@Override
	public void onError(String message) {

		if (message != null) {
			showSnackBar(message);
		} else {
			showSnackBar(getString(R.string.api_default_error));
		}	}


	/**
	 * Used by onError
	 * @param message Message to be shown
	 */
	private void showSnackBar(String message) {
		Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),
				message, Snackbar.LENGTH_LONG);
		View sbView = snackbar.getView();
		TextView textView = (TextView) sbView
				.findViewById(android.support.design.R.id.snackbar_text);
		textView.setTextColor(ContextCompat.getColor(this, R.color.colorPrimary));
		snackbar.show();
	}

	/** Check for internet connection
	 * @return a boolean
	 */
		@Override
	public boolean isNetworkConnected() {
		return NetworkUtils.isNetworkConnected(getApplicationContext());
	}


	public void hideKeyboard() {
		View view = this.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)
					getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}

	public void setUnBinder(Unbinder unBinder) {
		mUnBinder = unBinder;
	}


	@Override
	protected void onDestroy() {

		if (mUnBinder != null) {
			mUnBinder.unbind();
		}
		super.onDestroy();
	}

	protected abstract void setUp();
}
