package bg.devlabs.dev_lafka.ui.bill;

import bg.devlabs.dev_lafka.ui.base.LafkaPresenter;

/**
 * Created by Radoslav on 15-Mar-17.
 */

public interface BillPresenter<V extends BillView> extends LafkaPresenter<V> {
	/**
	 * Loads the user orders into the list
	 */
	void loadList();

	/**
	 * Loads the lafka's meals into the list
	 */
	void loadBuyList();

	/**
	 * Deletes a user's placed order
	 * @param orderId the ID of the ODER
	 * @param position the position of the order after the list
	 */
	void deleteItem(int orderId, int position);
	void selectProduct(String barcode);
}
