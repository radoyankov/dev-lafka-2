package bg.devlabs.dev_lafka.ui.shortcut;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.widget.Toast;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.Devlafka;
import bg.devlabs.dev_lafka.ui.base.BaseActivity;

/**
 * Created by Radoslav on 16-Mar-17.
 *
 * Activity for 7.1 app shortcut activity
 */

public class ShortcutActivity  extends BaseActivity implements ShortcutView{

	@Inject
	ShortcutPresenterImpl<ShortcutView> mPresenter;

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setUp();
	}

	@Override
	protected void setUp() {
		Devlafka.getAppComponent(this).inject(this);
		mPresenter.onAttach(this);
		mPresenter.makeCall();
		finish();
	}

	@Override
	public void requestPermission(String permission, Handler.Callback callback) {

	}

	@Override
	public void showToast(boolean isSuccess) {
		Toast.makeText(getApplicationContext(), isSuccess?"Success":"Error", Toast.LENGTH_SHORT).show();
	}
}
