package bg.devlabs.dev_lafka.ui.login;

import com.androidnetworking.error.ANError;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.data.DataManager;
import bg.devlabs.dev_lafka.ui.base.BasePresenter;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Radoslav on 08-Mar-17.
 *
 * Login logic
 */

public class LoginPresenterImpl<V extends LoginView> extends BasePresenter<V> implements LoginPresenter<V> {

	@Inject
	public LoginPresenterImpl(DataManager dataManager, CompositeDisposable compositeDisposable) {
		super(dataManager, compositeDisposable);
	}

	/**
	 * Attempts a login
	 * @param email User's email
	 * @param pass  User's pass
	 */
	@Override
	public void login(String email, String pass) {
		getDataManager().doLoginCall(email, pass)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(logResponseApiResponse -> {
					if (logResponseApiResponse.isSuccess()) {
						//opens the home screen after a successful login
							getLafkaView().openHomeScreen();
						//sets the local prefs
						getDataManager().setAccessToken(logResponseApiResponse.getData().getSessionId());
						getDataManager().setCurrentUserName(logResponseApiResponse.getData().getLastName());
					} else {
						//shows an error on login fail
							getLafkaView().showError();
					}
				}, throwable -> handleApiError(new ANError(throwable)));
	}
}
