package bg.devlabs.dev_lafka.ui.shortcut;

import bg.devlabs.dev_lafka.ui.base.LafkaPresenter;

/**
 * Created by Radoslav on 17-Mar-17.
 *
 * Presenter for 7.1 app shortcut handling
 */

public interface ShortcutPresenter<V extends ShortcutView> extends LafkaPresenter<V> {
	public void makeCall();

}
