package bg.devlabs.dev_lafka.ui.base;

import com.androidnetworking.error.ANError;

/**
 * Created by Radoslav on 08-Mar-17.
 */

public interface LafkaPresenter<V extends LafkaView> {

	void onAttach(V mvpView);

	void onDetach();

	/**
	 * Handles API Error
	 * @param error Error Object
	 */
	void handleApiError(ANError error);

	/**
	 * Sets user as logged off.
	 */
	void setUserAsLoggedOut();
}
