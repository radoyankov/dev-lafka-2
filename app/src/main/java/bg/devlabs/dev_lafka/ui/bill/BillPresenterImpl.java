package bg.devlabs.dev_lafka.ui.bill;

import com.androidnetworking.error.ANError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.data.DataManager;
import bg.devlabs.dev_lafka.data.network.model.OrdersResponse;
import bg.devlabs.dev_lafka.ui.base.BasePresenter;
import bg.devlabs.dev_lafka.utils.ProductsList;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Radoslav on 15-Mar-17.
 * <p>
 * Bill list logic
 */

public class BillPresenterImpl<V extends BillView> extends BasePresenter<V> implements BillPresenter<V> {

	@Inject
	public BillPresenterImpl(DataManager dataManager, CompositeDisposable compositeDisposable) {
		super(dataManager, compositeDisposable);
	}

	/**
	 * Loads the user's orders into the main list
	 */
	@Override
	public void loadList() {
		List<OrdersResponse> list = new ArrayList<>();
		getDataManager().doOrdersCall(getDataManager().getAccessToken(), "")
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.map(listApiResponse -> {
					if (listApiResponse.isSuccess()) {
						for (OrdersResponse item : listApiResponse.getData()) {
							if (!item.getPaid()) {
								list.add(item);
							}
						}
					} else {
						getLafkaView().showError();
					}
					return list;
				})
				.subscribe(ordersResponses -> {
					Collections.reverse(ordersResponses);
					getLafkaView().showList(ordersResponses);
				});

	}

	/**
	 * Loads the lafka's products into the main list
	 */
	@Override
	public void loadBuyList() {
		if (ProductsList.products == null) {
			getDataManager().doProductsCall(getDataManager().getAccessToken())
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(listApiResponse -> {
						if (listApiResponse.isSuccess()) {
							//sorts by quantity
							Collections.sort(listApiResponse.getData(), (lhs, rhs) -> Integer.parseInt(lhs.getQuantity()) > Integer.parseInt(rhs.getQuantity())
									? -1 : Integer.parseInt(lhs.getQuantity()) < Integer.parseInt(rhs.getQuantity()) ? 1 : 0);
							getLafkaView().showBuyList(listApiResponse.getData());
						}
					});
		} else {
			getLafkaView().showBuyList(ProductsList.products);
		}
	}


	/**
	 * Deletes a user's order
	 *
	 * @param orderId  the ID of the ODER
	 * @param position the position of the order after the list
	 */
	@Override
	public void deleteItem(int orderId, int position) {
		getDataManager().doDeleteCall(getDataManager().getAccessToken(), orderId)
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(apiResponse -> {
					if (apiResponse.isSuccess()) {
						getLafkaView().onItemDeleted(position);
					}
				}, throwable -> handleApiError(new ANError(throwable)));
	}

	/**
	 * Selects a product to bring to the main screen
	 *
	 * @param barcode barcode
	 */
	@Override
	public void selectProduct(String barcode) {
		getLafkaView().selectProduct(barcode);
	}
}
