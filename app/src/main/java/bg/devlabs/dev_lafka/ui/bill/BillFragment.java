package bg.devlabs.dev_lafka.ui.bill;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.jakewharton.rxbinding2.widget.RxTextView;

import java.util.List;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.Devlafka;
import bg.devlabs.dev_lafka.R;
import bg.devlabs.dev_lafka.data.network.model.OrdersResponse;
import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import bg.devlabs.dev_lafka.ui.base.BaseFragment;
import bg.devlabs.dev_lafka.ui.main.MainActivity;
import bg.devlabs.dev_lafka.utils.Not;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Radoslav on 15-Mar-17.
 */

public class BillFragment extends BaseFragment implements BillView {

	@Inject
	BillPresenterImpl<BillView> mPresenter;


	@BindView(R.id.list_bill)
	RecyclerView list;
	@BindView(R.id.edit_search)
	EditText search;

	BillAdapter adapter;
	//whether screen is orders list or all products list
	boolean isBuyScreen = false;

	public static BillFragment newInstance() {
		return new BillFragment();
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.card_bill, container, false);
		ButterKnife.bind(this, rootView);
		Devlafka.getAppComponent(getActivity()).inject(this);
		mPresenter.onAttach(this);
		setUp(rootView);
		return rootView;
	}

	/**
	 * @param orders Object with user's orders
	 */
	@Override
	public void showList(List<OrdersResponse> orders) {
		adapter = new BillAdapter(mPresenter, orders);
		showListBase();
	}

	/**
	 * @param products Object with lafka's products
	 */
	@Override
	public void showBuyList(List<ProductsResponse> products) {
		adapter = new BillAdapter(mPresenter, products, true, getContext());
		showListBase();
	}

	/**
	 * Sets the screen to one of the two lists
	 * @param isBuyScreen All products or user's orders list
	 */
	public void setIsBuyScreen(boolean isBuyScreen) {
		this.isBuyScreen = isBuyScreen;
		if (isBuyScreen) {
			//sets card animation direction
			direction = 2;
		}
	}

	/**
	 * base for the 2 methods
	 */
	public void showListBase() {
		list.setLayoutManager(new LinearLayoutManager(getContext()));
		list.setAdapter(adapter);
		list.invalidate();
		if (isBuyScreen) {
			//reverses the card's animation
			Not.now(() -> direction = 1);
			//filters the all product list based on text input
			search.setVisibility(View.VISIBLE);
			RxTextView.textChangeEvents(search)
					.subscribe(textViewTextChangeEvent -> {
						adapter.filterList(search.getText().toString());
					});

		} else {
			Not.now(() -> direction = 4);
		}
	}

	/**
	 * Shows error for product load fail
	 */
	@Override
	public void showError() {
		onError(R.string.error_list_load);
	}

	/**
	 * Deletes an order based on list position
	 * @param position The position
	 */
	@Override
	public void onItemDeleted(int position) {
		adapter.notifyItemRemoved(position);
		if (adapter.getItemCount() == 0) {
			((MainActivity) getActivity()).showHomeFragment(true);
		}
	}

	/** Selects a products for the home view based on barcode
	 * @param barcode the barcode of the item
	 */
	@Override
	public void selectProduct(String barcode) {
		((MainActivity) getActivity()).openHomeWithSelected(barcode);
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override
	protected void setUp(View view) {
		if (!isBuyScreen) {
			mPresenter.loadList();
		} else {
			mPresenter.loadBuyList();

		}
	}
}
