package bg.devlabs.dev_lafka.ui.login;

import bg.devlabs.dev_lafka.di.PerActivity;
import bg.devlabs.dev_lafka.ui.base.LafkaPresenter;

/**
 * Created by Radoslav on 06-Mar-17.
 */

@PerActivity
public interface LoginPresenter<V extends LoginView> extends LafkaPresenter<V> {

	/**
	 * Tries a login
	 * @param email User's email
	 * @param pass User's pass
	 */
	void login(String email, String pass);

}
