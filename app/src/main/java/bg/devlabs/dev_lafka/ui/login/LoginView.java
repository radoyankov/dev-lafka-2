package bg.devlabs.dev_lafka.ui.login;

import bg.devlabs.dev_lafka.ui.base.LafkaView;

/**
 * Created by Radoslav on 06-Mar-17.
 */

public interface LoginView extends LafkaView {

	/**
	 * Opens the main Home fragment
	 */
	void openHomeScreen();


	/**
	 * Shows a login error
	 */
	void showError();
}
