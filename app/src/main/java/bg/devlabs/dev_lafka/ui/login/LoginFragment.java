package bg.devlabs.dev_lafka.ui.login;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;

import com.jakewharton.rxbinding2.widget.RxTextView;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.Devlafka;
import bg.devlabs.dev_lafka.R;
import bg.devlabs.dev_lafka.ui.base.BaseFragment;
import bg.devlabs.dev_lafka.ui.main.MainActivity;
import bg.devlabs.dev_lafka.utils.AuthUtils;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Observable;

/**
 * Created by Radoslav on 08-Mar-17.
 */

public class LoginFragment extends BaseFragment implements LoginView {

	@Inject
	LoginPresenterImpl<LoginView> mPresenter;

	@BindView(R.id.image_logo_back)
	ImageView logo;
	@BindView(R.id.input_email)
	EditText inputEmail;
	@BindView(R.id.input_password)
	EditText inputPassword;
	@BindView(R.id.button_login)
	FloatingActionButton buttonAction;
	@BindView(R.id.login_card)
	CardView card;


	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.card_login, container, false);
		setUnBinder(ButterKnife.bind(this, rootView));
		Devlafka.getAppComponent(getActivity()).inject(this);
		mPresenter.onAttach(this);
		setUp(rootView);
		return rootView;
	}

	@Override
	protected void setUp(View view) {
		setupValidation();
		setupAnimation();
		setupEnterListener();
	}

	/**
	 * Sets up a listener for the DONE action on the keyboard
	 */
	private void setupEnterListener() {
		inputPassword.setOnEditorActionListener((v, actionId, event) ->{
			if (actionId == EditorInfo.IME_ACTION_DONE) {
				attemptLogin();
				return true;
			} else {
				return false;
			}
		});
	}

	@Override
	public void showError() {
		buttonAction.show();
		buttonAction.setClickable(true);
		setError(card, true);
	}

	@OnClick(R.id.button_login)
	public void onClick() {
		attemptLogin();
	}

	public void attemptLogin(){
		buttonAction.setClickable(false);
		hideKeyboard();
		mPresenter.login(inputEmail.getText().toString(), inputPassword.getText().toString());
		buttonAction.hide();
	}


	public static LoginFragment newInstance() {
		return new LoginFragment();
	}


	@Override
	public void openHomeScreen() {
		shouldAnimate = true;
		((MainActivity) getActivity()).showHomeFragment(true);
	}


	/**
	 * Sets up doughnut rotation
	 */
	private void setupAnimation() {
		Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.rotate);
		animation.setInterpolator(new LinearInterpolator());
		logo.setAnimation(animation);
	}

	/**
	 * RX validation of the email and password
	 */
	private void setupValidation() {
		Observable.combineLatest(RxTextView.textChangeEvents(inputEmail), RxTextView.textChangeEvents(inputPassword), (emailObservable, passwordObservable) -> {
			if (!TextUtils.isEmpty(emailObservable.text())
					&& AuthUtils.isValidEmail(emailObservable.text().toString())
					&& AuthUtils.isValidPass(passwordObservable.text().toString())) {
				buttonAction.show();
			} else buttonAction.hide();
			return true;
		}).subscribe();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
	}

	@Override
	public void onResume() {
		super.onResume();
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}




}
