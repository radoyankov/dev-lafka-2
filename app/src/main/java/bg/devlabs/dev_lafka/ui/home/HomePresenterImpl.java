package bg.devlabs.dev_lafka.ui.home;

import com.androidnetworking.error.ANError;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import bg.devlabs.dev_lafka.data.DataManager;
import bg.devlabs.dev_lafka.data.network.model.OrdersResponse;
import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import bg.devlabs.dev_lafka.ui.base.BasePresenter;
import bg.devlabs.dev_lafka.utils.ProductsList;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Radoslav on 08-Mar-17.
 */

public class HomePresenterImpl<V extends HomeView> extends BasePresenter<V> implements HomePresenter<V> {
	private List<ProductsResponse> responselist;
	private String selectedId;

	@Inject
	public HomePresenterImpl(DataManager dataManager, CompositeDisposable compositeDisposable) {
		super(dataManager, compositeDisposable);
	}

	/**
	 * Attempts a purchase
	 * @param id item's ID
	 */
	@Override
	public void purchase(String id) {
		if (id != null) {
			getDataManager().doMakeCall(getDataManager().getAccessToken(), Integer.parseInt(id), 1)
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(makeResponseApiResponse -> {
						if (makeResponseApiResponse.isSuccess()) {
							getLafkaView().showSuccess();
							getLafkaView().updateAutomatic();
							getDataManager().setLastItem(makeResponseApiResponse.getData().getProductId());
							getLafkaView().setupIconShortcut(ProductsList.getFromId(makeResponseApiResponse.getData().getProductId()).getName());
						} else {
							getLafkaView().onError(makeResponseApiResponse.getError());
						}
					}, throwable -> getLafkaView().showError());
		} else {
			getLafkaView().showError();
		}
	}

	/**
	 * Sets products
	 */
	@Override
	public void setProducts() {
		if (ProductsList.products == null) {
			getDataManager().doProductsCall(getDataManager().getAccessToken())
					.subscribeOn(Schedulers.io())
					.observeOn(AndroidSchedulers.mainThread())
					.subscribe(listApiResponse -> {
						if (listApiResponse.isSuccess()) {
							//sorts on quantity
							Collections.sort(listApiResponse.getData(), (lhs, rhs) -> Integer.parseInt(lhs.getQuantity()) > Integer.parseInt(rhs.getQuantity())
									? -1 : Integer.parseInt(lhs.getQuantity()) < Integer.parseInt(rhs.getQuantity()) ? 1 : 0);
							responselist = listApiResponse.getData();
							getLafkaView().showViews();
						} else {
							getLafkaView().onError(listApiResponse.getError());
						}
					}, throwable -> {
						getLafkaView().showError();
						getLafkaView().showViews();
					});
		} else {
			responselist = ProductsList.products;
			getLafkaView().showViews();
		}
	}

	/**
	 * @param id item's ID
	 */
	@Override
	public void setSelected(String id) {
		for (ProductsResponse item : responselist) {
			if (item.getBarcode().equals(id)) {
				selectedId = item.getId();
				getLafkaView().setSelected(item);
				return;
			}
		}

		//computes the closest barcode if no exact match is available
		int minIndex = 0, min = 1000, computed;
		for (int i = 0; i < responselist.size(); i++) {
			computed = levenshtein(id, responselist.get(i).getBarcode());
			if (min > computed) {
				min = computed;
				minIndex = i;
			}
		}
		selectedId = responselist.get(minIndex).getId();
		getLafkaView().setSelected(responselist.get(minIndex));
	}

	@Override
	public void onBarcodeClicked() {

	}

	@Override
	public void loadProducts() {
	}

	/**
	 * Loads the user's bill
	 */
	@Override
	public void loadBill() {
		getDataManager().doOrdersCall(getDataManager().getAccessToken(), "")
				.subscribeOn(Schedulers.io())
				.observeOn(AndroidSchedulers.mainThread())
				.map(listApiResponse -> {
					double sum = 0;
					if (listApiResponse.isSuccess()) {
						for (OrdersResponse item : listApiResponse.getData()) {
							//sums up all of user's items
							if (!item.getPaid()) {
								sum += Double.parseDouble(item.getPrice());
							}
						}
					} else {
						getLafkaView().onError(listApiResponse.getError());
					}
					return sum;
				})
				.subscribe(sum -> getLafkaView().updateBill(sum), throwable -> handleApiError(new ANError(throwable)));
	}

	@Override
	public void onShake() {
		//gets user's last name on shake for the toast
		getLafkaView().onShake(getDataManager().getCurrentUserName().toLowerCase());
	}


	@Override
	public void logout() {
		setUserAsLoggedOut();
		getLafkaView().openHomeScreen();
	}

	/**
	 * Calculates closest correct barcode
	 */
	private static int minimum(int a, int b, int c) {
		return Math.min(Math.min(a, b), c);
	}


	/**
	 * Calculates closest correct barcode
	 */
	public static int levenshtein(CharSequence lhs, CharSequence rhs) {
		int[][] distance = new int[lhs.length() + 1][rhs.length() + 1];

		for (int i = 0; i <= lhs.length(); i++)
			distance[i][0] = i;
		for (int j = 1; j <= rhs.length(); j++)
			distance[0][j] = j;

		for (int i = 1; i <= lhs.length(); i++)
			for (int j = 1; j <= rhs.length(); j++)
				distance[i][j] = minimum(
						distance[i - 1][j] + 1,
						distance[i][j - 1] + 1,
						distance[i - 1][j - 1] + ((lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1));

		return distance[lhs.length()][rhs.length()];
	}

}
