package bg.devlabs.dev_lafka.ui.main;

import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.ImageView;


import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;


import javax.inject.Inject;

import bg.devlabs.dev_lafka.Devlafka;
import bg.devlabs.dev_lafka.R;
import bg.devlabs.dev_lafka.ui.base.BaseActivity;
import bg.devlabs.dev_lafka.ui.bill.BillFragment;
import bg.devlabs.dev_lafka.ui.home.HomeFragment;
import bg.devlabs.dev_lafka.ui.login.LoginFragment;
import bg.devlabs.dev_lafka.utils.FlipAnimation;
import bg.devlabs.dev_lafka.utils.Not;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements MainView {
	@Inject
	MainPresenterImpl<MainView> mPresenter;

	@BindView(R.id.fragment_home)
	FrameLayout frameLayout;
	@BindView(R.id.image_background)
	ImageView background;


	HomeFragment homeFragment;

	//Whether user is after bill screen and should overwrite the back pressed button
	boolean shouldShowHome = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		Devlafka.getAppComponent(this).inject(this);
		setUnBinder(ButterKnife.bind(this));
		//initial check for internet connection
		if (isNetworkConnected()) {
			setUp();
		} else {
			onError("No internet Connection");
		}
	}

	/**
	 * Randomly sets the background to one of 2 images
	 */
	private void setBackground() {
		if (Math.random() > 0.5) {
			background.setImageDrawable(getResources().getDrawable(R.drawable.background));
		} else {
			background.setImageDrawable(getResources().getDrawable(R.drawable.background1));
		}
	}

	@Override
	protected void setUp() {
		setBackground();
		mPresenter.onAttach(this);
		//checks if user is logged after
		mPresenter.checkLogin();
	}


	/**
	 * Shows home screen
	 *
	 * @param shouldAnimate Whether it should be shown with an animation
	 */
	@Override
	public void showHomeFragment(boolean shouldAnimate) {
		if (!shouldShowHome) {
			this.homeFragment = HomeFragment.newInstance();
			homeFragment.setShouldAnimate(shouldAnimate, FlipAnimation.RIGHT);
		}
		//resets the scale of the card
		frameLayout.animate().scaleY(1.0f).scaleX(1.0f).translationY(0.0f).setDuration(200);
		setFragment(homeFragment, !shouldAnimate);
	}

	/**
	 * Shows login screen
	 *
	 * @param shouldAnimate Whether it should be shown with an animation
	 */
	@Override
	public void showLoginFragment(boolean shouldAnimate) {
		LoginFragment loginFragment = LoginFragment.newInstance();
		loginFragment.setShouldAnimate(shouldAnimate, FlipAnimation.RIGHT);
		setFragment(loginFragment, !shouldAnimate);
	}

	/**
	 * Shows Bill screen
	 *
	 * @param isBuyScreen Whether it should show the full products list or the user's order list
	 */
	@Override
	public void showBillFragment(boolean isBuyScreen) {
		BillFragment billFragment = BillFragment.newInstance();
		billFragment.setIsBuyScreen(isBuyScreen);
		shouldShowHome = true;
		if (isBuyScreen) {
			//reverses the animation of screen for when the user is returning
			Not.now(() -> homeFragment.setDirection(1));
			frameLayout.animate().scaleY(1.1f).scaleX(1.1f).translationY(80f).setDuration(200);
		} else {
			Not.now(() -> homeFragment.setDirection(4));
		}
		setFragment(billFragment, false);
	}

	/**
	 * Shows home with already selected item
	 *
	 * @param barcode the selected item's barcode
	 */
	@Override
	public void openHomeWithSelected(String barcode) {
		homeFragment.openWithSelected(barcode);
		showHomeFragment(true);
	}

	public void setFragment(Fragment fragment, boolean shouldFade) {
		if (shouldFade) {
			frameLayout.setAlpha(0.0f);
			frameLayout.animate().alpha(1.0f).setDuration(600);
		}
		getSupportFragmentManager()
				.beginTransaction()
				.replace(R.id.fragment_home, fragment)
				.commit();
	}

	public void setShouldShowHome(boolean shouldShowHome) {
		this.shouldShowHome = shouldShowHome;
	}

	@Override
	public void onBackPressed() {
		if (shouldShowHome) {
			showHomeFragment(true);
			shouldShowHome = false;
		} else {
			super.onBackPressed();
		}
	}



	/**
	 * Dexter permissions
	 *
	 * @param permission Permission's string
	 * @param callback   Handles user's aciton
	 */
	@Override
	public void requestPermission(String permission, Handler.Callback callback) {
		Dexter.withActivity(this)
				.withPermission(permission)
				.withListener(new PermissionListener() {
					@Override
					public void onPermissionGranted(PermissionGrantedResponse response) {
						Bundle bundle = new Bundle(1);
						bundle.putBoolean("permission", true);
						Message message = new Message();
						message.setData(bundle);
						callback.handleMessage(message);
					}

					@Override
					public void onPermissionDenied(PermissionDeniedResponse response) {
						Bundle bundle = new Bundle(1);
						bundle.putBoolean("permission", false);
						Message message = new Message();
						message.setData(bundle);
						callback.handleMessage(message);
					}

					@Override
					public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
						token.continuePermissionRequest();
					}
				}).check();
	}
}
