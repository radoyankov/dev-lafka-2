package bg.devlabs.dev_lafka.ui.bill;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bg.devlabs.dev_lafka.R;
import bg.devlabs.dev_lafka.data.network.model.OrdersResponse;
import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;
import bg.devlabs.dev_lafka.utils.LoadableImageView;
import bg.devlabs.dev_lafka.utils.Not;
import bg.devlabs.dev_lafka.utils.ProductsList;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by Radoslav on 15-Mar-17.
 */

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.ViewHolder> {

	BillPresenterImpl<BillView> mPresenter;
	//user orders list
	List<OrdersResponse> list;
	//fill products list
	List<ProductsResponse> buyList;
	//filtered from the search box
	List<ProductsResponse> filteredList;
	Context context;
	//whether screen is user oders or full products list
	boolean isBuying = false;

	//whether list is currently being USED
	boolean isBlocked = false;

	public BillAdapter(BillPresenterImpl<BillView> mPresenter, List<OrdersResponse> list) {
		this.mPresenter = mPresenter;
		this.list = list;
	}

	public BillAdapter(BillPresenterImpl<BillView> mPresenter, List<ProductsResponse> list, boolean isBuying, Context context) {
		this.mPresenter = mPresenter;
		filteredList = buyList = list;
		this.isBuying = isBuying;
		this.context = context;
	}

	/**
	 * Filters lafka products list
	 *
	 * @param text String from the searchbox
	 */
	public void filterList(String text) {
		filteredList = new ArrayList<>();
		Observable<List<ProductsResponse>> observable = Observable.fromArray(buyList);
		observable.flatMap(new Function<List<ProductsResponse>, Observable<ProductsResponse>>() {
			@Override
			public Observable<ProductsResponse> apply(List<ProductsResponse> productsResponses) throws Exception {
				return Observable.fromIterable(productsResponses);
			}
		}).filter(productsResponse -> (productsResponse.getName().toLowerCase().contains(text.toLowerCase())))
				.subscribe(productsResponse -> filteredList.add(productsResponse), Throwable::printStackTrace, this::notifyDataSetChanged);

	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		LayoutInflater inflater = LayoutInflater.from(parent.getContext());
		View root = inflater.inflate(R.layout.list_item_bill, parent, false);
		return new ViewHolder(root);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.image.load(getCurrentProduct(position).getBarcode());
		holder.title.setText(getCurrentProduct(position).getName());
		if (!isBuying) {
			holder.date.setText(list.get(position).getCreatedAt());
			//sets up remove order button.
			holder.remove.setOnClickListener(v -> {
				if (!isBlocked) {
					mPresenter.deleteItem(Integer.parseInt(list.get(position).getId()), position);
					list.remove(position);
					setItems();
				}


			});
		} else {
			//check for quantity and sets up color accordingly
			if (getCurrentProduct(position).getQuantity().equals("0")) {
				holder.date.setText(R.string.error_quantity);
				holder.date.setTextColor(context.getResources().getColor(R.color.light_red));
			} else {
				holder.date.setText(getCurrentProduct(position).getPrice() + context.getString(R.string.lv_dor));
				holder.date.setTextColor(context.getResources().getColor(R.color.green));
			}
			holder.remove.setAlpha(0.0f);
			//sets up select item listener
			Not.now(500, () ->
					holder.layout.setOnClickListener(v -> mPresenter.selectProduct(getCurrentProduct(position).getBarcode())));


		}

	}

	public void setItems() {
		isBlocked = true;
		Not.now(1000, () -> {
			notifyDataSetChanged();
			isBlocked = false;
		});
	}

	@Override
	public int getItemCount() {
		if (!isBuying) {
			return list.size();
		}
		return filteredList.size();
	}

	private ProductsResponse getCurrentProduct(int pos) {
		if (!isBuying) {
			return ProductsList.getFromId(list.get(pos).getProductId());
		}
		return ProductsList.getFromId(filteredList.get(pos).getId());
	}

	static class ViewHolder extends RecyclerView.ViewHolder {

		@BindView(R.id.list_item_image)
		LoadableImageView image;
		@BindView(R.id.list_item_title)
		TextView title;
		@BindView(R.id.list_item_date)
		TextView date;
		@BindView(R.id.list_item_remove)
		ImageView remove;
		@BindView(R.id.list_item_layout)
		RelativeLayout layout;

		public ViewHolder(View view) {
			super(view);
			ButterKnife.bind(this, view);
		}
	}
}
