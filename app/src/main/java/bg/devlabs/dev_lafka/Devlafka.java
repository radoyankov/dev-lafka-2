package bg.devlabs.dev_lafka;

import android.app.Application;
import android.content.Context;

import bg.devlabs.dev_lafka.di.component.ApplicationComponent;
import bg.devlabs.dev_lafka.di.component.DaggerApplicationComponent;
import bg.devlabs.dev_lafka.di.module.ApplicationModule;

/**
 * Created by Radoslav on 06-Mar-17.
 *
 * DefLafka v2.0
 *
 */

public class Devlafka extends Application {


	private ApplicationComponent mApplicationComponent;

	@Override
	public void onCreate() {
		super.onCreate();

//		mApplicationComponent = DaggerApplicationComponent.builder()
//				.applicationModule(new ApplicationModule(this)).build();

//		mApplicationComponent.inject(this);


	}

	protected ApplicationComponent createComponent() {
		return DaggerApplicationComponent.builder()
				.applicationModule(new ApplicationModule(this))
				.build();
	}

	public static ApplicationComponent getAppComponent(Context context) {
		Devlafka app = (Devlafka) context.getApplicationContext();
		if (app.mApplicationComponent == null) {
			app.mApplicationComponent = app.createComponent();
		}
		return app.mApplicationComponent;
	}

	public ApplicationComponent getComponent() {
		return mApplicationComponent;
	}

	public void setComponent(ApplicationComponent applicationComponent) {
		mApplicationComponent = applicationComponent;
	}
}
