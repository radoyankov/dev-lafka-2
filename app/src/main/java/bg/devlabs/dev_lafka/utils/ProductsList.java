package bg.devlabs.dev_lafka.utils;

import java.util.List;

import bg.devlabs.dev_lafka.data.network.model.ProductsResponse;

/**
 * Created by Radoslav on 15-Mar-17.
 *
 * Class for storing all of lafka's products for the current session
 */

public class ProductsList {
	public static List<ProductsResponse> products;

	/**
	 * Gets an item with provided ID
	 * @param id The ID of the item
	 * @return a variable type ProductsResponse
	 */
	public static ProductsResponse getFromId(String id) {
		if (products != null) {
			for (ProductsResponse item : products) {
				if (item.getId().equals(id)) {
					return item;
				}
			}
		}
		return null;
	}
}
