package bg.devlabs.dev_lafka.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.TypedValue;

import me.dm7.barcodescanner.core.ViewFinderView;

/**
 * Created by Radoslav on 10-Mar-17.
 *
 * Custom view for the barcode scanner
 */

public class CustomViewFinderView extends ViewFinderView {
	public static final String TRADE_MARK_TEXT = "ZXing";
	public static final int TRADE_MARK_TEXT_SIZE_SP = 40;
	public final Paint PAINT = new Paint();

	public CustomViewFinderView(Context context) {
		super(context);
		init();
	}

	public CustomViewFinderView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		PAINT.setColor(Color.WHITE);
		PAINT.setAntiAlias(true);
		float textPixelSize = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP,
				TRADE_MARK_TEXT_SIZE_SP, getResources().getDisplayMetrics());
		PAINT.setTextSize(textPixelSize);
		setSquareViewFinder(true);
	}

	@Override
	public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

	}
}
