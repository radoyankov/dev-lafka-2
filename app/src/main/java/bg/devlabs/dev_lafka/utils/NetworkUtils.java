package bg.devlabs.dev_lafka.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Radoslav on 08-Mar-17.
 *
 * Network Utilities
 */

public class NetworkUtils {
	private NetworkUtils() {
	}

	/**
	 * Checks whether there is an internet connection
	 * @param context context of app
	 * @return a boolean
	 */
	public static boolean isNetworkConnected(Context context) {
		ConnectivityManager cm =
				(ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}
}
